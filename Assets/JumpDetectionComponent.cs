﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpDetectionComponent : MonoBehaviour {

    public bool canJump = true;

    void OnTriggerStay2D(Collider2D collider)
    {
        canJump = true;
    }
}
