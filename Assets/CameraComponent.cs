﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraComponent : MonoBehaviour {

    PlaneController plane;
    SoldierController soldier;

    Transform target;

    Vector3 speed = Vector3.zero;

    void Start()
    {
        plane = GameObject.Find("Plane").GetComponent<PlaneController>();
        soldier = GameObject.Find("Soldier").GetComponent<SoldierController>();
    }

    void Update()
    {
        if (plane != null && plane.controlled && target != plane.transform)
        {
           target = plane.transform;
            speed = Vector3.zero;
        }
        else if (soldier != null && soldier.controlled && target != soldier.transform)
        {
            target = soldier.transform;
            speed = Vector3.zero;
        }

        if (plane != null && soldier != null)
        {
            transform.position = Vector3.SmoothDamp(
                transform.position,
                new Vector3(target.position.x, transform.position.y, transform.position.z),
                ref speed, .3f);
        }
          
    }
}
