﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCComponent : MonoBehaviour {

    public bool enableMovement;

    private const int TRIGGER_DISTANCE = 50;
    private const int SPEED = 5;

    private bool hasStartedMoving = false;

    protected Transform planeTransform;
    protected Transform soldierTransform;

    private Rigidbody2D r;

    public void Start ()
    {
        planeTransform = GameObject.Find("Plane").transform;
        soldierTransform = GameObject.Find("Soldier").transform;
        r = GetComponent<Rigidbody2D>();
    }

	void Update () {
        if (enableMovement)
        {
            if (!hasStartedMoving)
            {
                if (IsInRange(planeTransform) || IsInRange(soldierTransform))
                {
                    hasStartedMoving = true;
                }
            }
            else
            {
                r.velocity = new Vector2(-SPEED, r.velocity.y);
            }
        }
	}

    bool IsInRange(Transform otherTransform)
    {
        return Mathf.Max(transform.position.x - otherTransform.position.x) < TRIGGER_DISTANCE;
    }
}
