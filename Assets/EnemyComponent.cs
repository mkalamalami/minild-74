﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyComponent : NPCComponent {

    private GameController gameController;

    private List<GameObject> currentCollisions = new List<GameObject>();

    private bool exploded = false;

    new void Start()
    {
        base.Start();
        gameController = GameObject.Find("Game").GetComponent<GameController>();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.name == "Soldier" || collider.gameObject.name == "Plane")
        {
            if (!exploded)
            {
                exploded = true;
                GetComponent<ParticleSystem>().Play();
                Destroy(transform.parent.GetComponent<SpriteRenderer>());
                Destroy(collider.gameObject);
                foreach (GameObject other in currentCollisions)
                {
                    Destroy(other, 0.2f);
                }
                currentCollisions = new List<GameObject>();
                gameController.Defeat();
            }
        }
        else if (collider.gameObject.layer != 10) { // Undestructible
            currentCollisions.Add(collider.gameObject);
         }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        currentCollisions.Remove(collider.gameObject);
    }
}
