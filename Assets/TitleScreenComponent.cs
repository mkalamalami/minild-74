﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class TitleScreenComponent : MonoBehaviour {

    private GameStateComponent gameState;

    void Start () {

        // MAX SCORE DB
        List<int> maxScores = new List<int>();
        maxScores.Add(2); // Level 1
        maxScores.Add(9);
        maxScores.Add(7);

        int totalCivilians = 0;
        foreach (int score in maxScores) totalCivilians += score;
        
        gameState = GameObject.Find("GameState").GetComponent<GameStateComponent>();

        foreach (GameObject levelLabelObj in GameObject.FindGameObjectsWithTag("LevelLabel"))
        {
            Text levelLabel = levelLabelObj.GetComponent<Text>();
            int levelId = int.Parse(levelLabel.text.Split(' ')[1]);
            levelLabel.text += gameState.GetCiviliansSaved(levelId) + " / " + maxScores[levelId - 1];
        }

        GameObject.Find("Total").GetComponent<Text>().text += 
            gameState.GetTotalCiviliansSaved() + " / " + totalCivilians;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ExitGame()
    {
        Application.Quit();
    }

    public void StartLevel(int level)
    {
        gameState.currentLevel = level;
        SceneManager.LoadScene("Level" + level);
    }
}
