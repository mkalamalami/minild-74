﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YouComponent : MonoBehaviour {

    PlaneController plane;
    SoldierController soldier;

    void Start () {
        plane = GameObject.Find("Plane").GetComponent<PlaneController>();
        soldier = GameObject.Find("Soldier").GetComponent<SoldierController>();
    }
	
	void Update () {
        if (plane.controlled && transform.parent != plane.transform)
        {
            transform.SetParent(plane.transform);
            transform.localPosition = new Vector3(0, 4, 0);
        } else if (soldier.controlled && transform.parent != soldier.transform)
        {
            transform.SetParent(soldier.transform);
            transform.localPosition = new Vector3(0, 6, 0);
        }
	}
}
