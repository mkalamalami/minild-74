﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneController : MonoBehaviour {

    public GameObject bulletPrefab;

    public bool controlled;

    public static int SPEED = 10; 

    private Rigidbody2D r;

	void Start ()
    {
        r = GetComponent<Rigidbody2D>();
    }
	
	void Update () {
        if (controlled)
        {
            float h = Input.GetAxis("Horizontal");
            r.velocity = new Vector2(h * SPEED, 0);

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                FireBullet();
            }
        }
	}

    public void FireBullet()
    {
        Instantiate(bulletPrefab, transform.position, Quaternion.identity);
    }

    public void ToggleControlled()
    {
        this.controlled = !this.controlled;
    }
}
