﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxComponent : MonoBehaviour {

    public float ratio;

    private float initialX;

	void Start () {
        initialX = transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
        float mouseX = Screen.width / 2 - Input.mousePosition.x;
        transform.position = new Vector3(initialX + ratio * mouseX, transform.position.y, transform.position.z);
	}
}
